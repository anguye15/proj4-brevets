"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    start = arrow.get(brevet_start_time)

    if control_dist_km > brevet_dist_km:
    	control_dist_km = brevet_dist_km

    if control_dist_km <= 200:
    	shift = control_dist_km/34
    elif (control_dist_km > 200) and (control_dist_km <= 400):
    	shift = ((control_dist_km-200)/32) + (200/34)
    elif (control_dist_km > 400) and (control_dist_km <= 600):
    	shift = ((control_dist_km-400)/30) + (200/34) + (200/32)
    elif (control_dist_km > 600) and (control_dist_km <= 1000):
    	shift = ((control_dist_km-600)/28) + (200/34) + (200/32) + (200/30)
    elif (control_dist_km > 1000) and (control_dist_km <= 1300):
    	shift = ((control_dist_km-1000)/26) + (200/34) + (200/32) + (400/28)
        
    shift_hours = int(shift)
    shift_minutes = round(((shift-int(shift))*60))
    start = start.shift(hours=+shift_hours, minutes=+shift_minutes)

    return start.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    end = arrow.get(brevet_start_time)

    if control_dist_km > brevet_dist_km:
    	control_dist_km = brevet_dist_km

    if control_dist_km <= 60:
        shift = (control_dist_km/20) + 1
    elif control_dist_km == brevet_dist_km:
    	if control_dist_km == 200:
    		shift = 13.5
    	elif control_dist_km == 300:
    		shift = 20
    	elif control_dist_km == 400:
    		shift = 27
    	elif control_dist_km == 600:
    		shift = 40
    	elif control_dist_km == 1000:
    		shift = 75
    else:
	    if control_dist_km <= 600:
	        shift = control_dist_km/15
	    elif (control_dist_km > 600) and (control_dist_km <= 1000):
	        shift = ((control_dist_km-600)/11.428) + (600/15)
	    elif (control_dist_km > 1000) and (control_dist_km <= 1300):
	        shift = ((control_dist_km-1000)/13.333) + (400/11.428) + (600/15)

    shift_hours = int(shift)
    shift_minutes = round(((shift-int(shift))*60))
    end = end.shift(hours=+shift_hours, minutes=+shift_minutes)

    return end.isoformat()
