"""
Nose Tests for acp_times.py
"""
import nose
from acp_times import *

def test_simple():
	"""
	Simple test examples
	"""
	assert open_time(50, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T01:28:00+00:00"
	assert close_time(50, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T03:30:00+00:00"

def test_oddities():
	"""
	Testing French Algorithm
	"""
	assert close_time(0, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T01:00:00+00:00"
	assert close_time(20, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T02:00:00+00:00"

def test_rules():
	"""
	Testing Ariticle 9
	"""
	assert close_time(200, 200, "2017-01-01T00:00:00+00:00") == "2017-01-01T13:30:00+00:00"
	assert close_time(300, 300, "2017-01-01T00:00:00+00:00") == "2017-01-01T20:00:00+00:00"
	assert close_time(400, 400, "2017-01-01T00:00:00+00:00") == "2017-01-02T03:00:00+00:00"
	assert close_time(600, 600, "2017-01-01T00:00:00+00:00") == "2017-01-02T16:00:00+00:00"
	assert close_time(1000, 1000, "2017-01-01T00:00:00+00:00") == "2017-01-04T03:00:00+00:00"

def test_above():
	"""
	Testing control point beyond brevet's distance
	"""
	assert open_time(600, 400, "2017-01-01T00:00:00+00:00") == "2017-01-01T12:08:00+00:00"
	assert close_time(600, 400, "2017-01-01T00:00:00+00:00") == "2017-01-02T03:00:00+00:00"

def test_complex():
	"""
	Testing more complex examples
	"""
	assert close_time(200, 400, "2017-01-01T00:00:00+00:00") == "2017-01-01T13:20:00+00:00"
	assert open_time(880, 1000, "2017-01-01T00:00:00+00:00") == "2017-01-02T04:48:00+00:00"
	assert close_time(750, 1000, "2017-01-01T00:00:00+00:00") == "2017-01-03T05:08:00+00:00"
