# Brevet Times Calculator

The Brevet Times Calculator is based on RUSA ACP controle time calculator(https://rusa.org/octime_acp.html).

Credits to Michal Young for the initial version of this code.

## Distance, Speed, Time Calculation

The brevet times calculator is based on ACP-sanctioned brevets between 200 and 1000 kilometers. Use the drop-down list to select a distance of 200km, 400km, 600km, or 1000km.

The speed used to calculate the times are based on the table in "ACP Brevet Control Calculator"(https://rusa.org/pages/acp-brevet-control-times-calculator).

The times are calculated based on the control's location, brevet's distance, and speed. In particular, the opening times are based the maximum speed of the control's location, and the closing times are based on the minimum speed of the control's location. For instance, consider a 200km brevet with controls at 60km, 120km, 175km, and at the finish (205km). The way the time is calculated at each control point is like so: 60/34, 120/34, 175/34, 200/34. However, there are exceptions to this rule. The first exception is that there are time limits for certain brevet distance. These are: (in hours and minutes, HH:MM) 13:30 for 200 KM, 20:00 for 300 KM, 27:00 for 400 KM, 40:00 for 600 KM, and 75:00 for 1000 KM. The second exception is that
the the time limit for a control within the first 60km is calculated differently. The control is divided by 20km/h and added 1 hour. The final exception, is how the times are calculated from a control beyond the brevet's distance. In general, the last control point can't be longer than 20 percent of the brevet's distance. However, if the last control is slightly longer, it is essentially irrelevant. The calculator will consider the brevet's distance as the last control point.

## Testing

The functions in acp_times.py are avaialble to be tested using python unit testing "nosetest". Use the following command while in a container to test the functions:

 ```
 docker exec -it <container_id> nosetests --tests=<file>
 ```

## Comments

More information about brevets can be found at (https://rusa.org/pages/rulesForRiders).

Author: Anthony Nguyen, anguye15@uoregon.edu


